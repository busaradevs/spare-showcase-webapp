// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

// Third party
// Apps
import './assets/sass/app.scss'
import Vue from 'vue'
import App from './App'
import router from './router'
import AtUI from 'at-ui'
import 'at-ui-style'

// Load
Vue.use(AtUI)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { 
  	App
  }
})
