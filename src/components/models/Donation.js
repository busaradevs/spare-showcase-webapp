export default function () {
  this.request_amount = 5 // minimum
  this.currency = 'USD'
  this.email = ''
  this.message = ''
  this.use_existing_default_card = false
  this.remember_card = false
}
