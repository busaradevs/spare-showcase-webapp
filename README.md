# spare-showcase-app

This repo hosts the spare sample webapp that demonstrates how to build a charity app using the Spare API.
Built using Vue.js.


## Folder Structure

```
build/                       webpack build assets
config/                      webpack config settings
dist/                        built project 
node_modules/                project dependancies
src/                         project source code
|- assets/					 app assets
|  +- sass 					 sass files
|- components/               app components
|  +- ...
|  +- ...                    
|- router/                   
|  +- index.js 				 app routes
|- App.vue 					 main vue component
|- main.js 					 main vue config file
static						 static files
|  +- img 					 app assets
|     +- ... 				 app images
package.json                 package file
index.html                	 html entry point
README.md                	 read.me file

```

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## Pre-requisites

You will need the following:

1. Spare Oauth App setup. You can register one from your Spare Account.
2. For testing, create a Spare Oauth app on our staging server


### Create an account on our staging server

Make a `POST` to `v1/users/users/` with:

JSON
```
{
    "email": "johndoe@email.com",
    "skip_email": true,
    "first_name": "John",
    "last_name": "Doe",
    "password": "password"
}
```

This call will return your details

### Create a SpareApp on our staging server

Make a `POST` to `v1/users/applications` with:

JSON
```
{
	"name": "MyAppName",
	"web_link": "http://myapplink.com",
	"authorization_grant_type": "client-credentials",
	"client_type": "confidential"
}

```

This call will return your app details including your `client_id` & `client_secret`.
Store these safefly as you will require them for request a token from the API.





